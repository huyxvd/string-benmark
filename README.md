1. Sao chép project từ liên kết này

```bash
git clone https://gitlab.com/huyxvd/string-benmark
```

2. Cài đặt các gói NuGet: Tải các gói cần thiết bằng lệnh sau:

```bash
dotnet restore
```

3. Xem xét mã nguồn và chạy chương trình để hiểu cách hoạt động.
4. Build chương trình ở chế độ Release để tối ưu hóa(*)

```bash
dotnet build -c Release
```

5. Thực hiện kiểm tra hiệu suất bằng cách chạy file `dll` đã build

```bash
dotnet StringBenmark\bin\Release\net8.0\StringBenmark.dll
```

6. Đợi kết quả kiểm tra và phân tích hiệu suất.