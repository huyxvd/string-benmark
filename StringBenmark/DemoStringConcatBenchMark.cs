﻿using BenchmarkDotNet.Attributes;
using System.Text;

[MemoryDiagnoser]
public class DemoStringConcatBenchMark
{
    const int NumberOfLoop = 100;

    [Benchmark]
    public string UsingPlusString()
    {
        string result = "";
        for (int i = 0; i < NumberOfLoop; i++)
        {
            result += i;
        }
        return result;
    }

    [Benchmark]
    public string UsingStringBuilder()
    {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < NumberOfLoop; i++)
        {
            builder.Append(i);
        }
        return builder.ToString();
    }

}
